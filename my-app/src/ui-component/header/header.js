import React from 'react';
import PropTypes from 'prop-types';

const header = (props) => {
  return (
      <>
        <header className={ props.className } data-test="header-component">
          <h1>{ props.pageLabel }</h1>
        </header>
      </>
    );
};

header.defaultProps = {
  className: "",
  pageLabel: ""
};

header.propTypes = {
  className: PropTypes.string.isRequired,
  pageLabel: PropTypes.string.isRequired
};

export default header;
