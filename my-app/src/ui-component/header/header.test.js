import React from 'react';
import Header from './header';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Header Component', () => {
  let wrapper;
  beforeEach(() => {
  	let props = {
      className: "page-header",
      pageLabel: "Test Header"
    };
    wrapper = shallow(<Header {...props} />);
  });

  it("Should Render Header", () => {
    const ele = test.findByAttr(wrapper, "header-component");
    expect(ele.length).toBe(1);
  });

  it("Should Render Header with Class Name", () => {
    const ele = test.findByVariableAttr(wrapper, "className", "page-header");
    expect(ele.length).toBe(1);
  });

});
