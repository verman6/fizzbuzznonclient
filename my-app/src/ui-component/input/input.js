import React from 'react';
import PropTypes from 'prop-types';

const Input = (data) => {
  const { name, type, className, style, onChange, labetText } = data;
    return (
      <React.Fragment>
        {labetText !== "" &&
          <h2>{labetText}</h2>
        }
        <input
          name={name}
          type={type}
          className={className}
          style={style}
          onChange={onChange}
          data-test="input-component"
        />
      </React.Fragment>
    );
};

Input.defaultProps = {
  value: "",
  name: "user-input",
  type: "text",
  className: "",
  style: null,
  labetText: "",
  onChange: () => { }
};

Input.propTypes = {
  name: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]),
  labetText: PropTypes.string,
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
  style: PropTypes.object,
  onChange: PropTypes.func
};

export default Input;