import React from 'react';
import Input from './input';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Input Component', () => {
  let wrapper;
  const onChangeMock = jest.fn();
  const event = {
    target: { value: 'the-value' }
  };
  beforeEach(() => {
    let props = {
      name: "fizzbuzzInput",
      type: "text",
      className: "input"
    };
    wrapper = shallow(<Input {...props} />);
  });

  it("Should Render Input Field", () => {
    const ele = test.findByAttr(wrapper, "input-component");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Name", () => {
    const ele = test.findByVariableAttr(wrapper, "name", "fizzbuzzInput");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Type", () => {
    const ele = test.findByVariableAttr(wrapper, "type", "text");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Class Name", () => {
    const ele = test.findByVariableAttr(wrapper, "className", "input");
    expect(ele.length).toBe(1);
  });

  it('Should Trigger onChange', () => {
    const component = shallow(<Input onChange={onChangeMock} />);
    component.find('input').simulate('change', event);
    expect(onChangeMock).toBeCalledWith(event);
  });

});