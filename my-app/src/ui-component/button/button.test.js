import React from 'react';
import Button from './button';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Button Component', () => {
  let wrapper;
  const onClickMock = jest.fn();
  beforeEach(() => {
  	let props = {
      name: "fizzbuzzBtn",
      type: "button",
      className: "button",
      onClick: onClickMock
    };
    wrapper = shallow(<Button {...props} />);
  });

  it("Should Render Submit Button", () => {
    const ele = test.findByAttr(wrapper, "btn-component");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Name", () => {
    const ele = test.findByVariableAttr(wrapper, "name", "fizzbuzzBtn");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Type", () => {
    const ele = test.findByVariableAttr(wrapper, "type", "button");
    expect(ele.length).toBe(1);
  });

  it("Should Render Button with Class Name", () => {
    const ele = test.findByVariableAttr(wrapper, "className", "button");
    expect(ele.length).toBe(1);
  });

  it('Should trigger onClick', () => {
    const ele = test.findByAttr(wrapper, 'btn-component');
    ele.simulate('click');
    expect(onClickMock).toBeCalled();
  });

});
