import React from 'react';
import PropTypes from 'prop-types';

const Button = (data) => {
  const { name, type, className, style, onClick, text } = data;
    return (
      <button
        name={name}
        type={type}
        className={className}
        style={style}
        onClick={onClick}
        data-test="btn-component"
      >
        {text}
      </button>
    );
};

Button.defaultProps = {
  name: "user-button",
  type: "button",
  className: "",
  style: null,
  text: "Submit",
  onClick: () => { }
};

Button.propTypes = {
  name: PropTypes.string,
  type: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  style: PropTypes.object,
  text: PropTypes.string,
  onClick: PropTypes.func
};

export default Button;