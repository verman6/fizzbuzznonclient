export function findByAttr(wrapper, attr) {
  return wrapper.find(`[data-test="${attr}"]`);
}

export function findByVariableAttr(wrapper, attrname, value) {
  return wrapper.find(`[${attrname}="${value}"]`);
}

export function findChild(wrapper, childName) {
  return wrapper.find(childName);
}