import React from 'react';
import Error from './error';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Error Component', () => {
  let wrapper;
  beforeEach(() => {
  	let props = {
      errorMessage: "Please enter number between 1 and 2000.",
      errorclassName: "error"
    };
    wrapper = shallow(<Error className={props.errorclassName} errorMessage={props.errorMessage} />);
  });

  it("Should Render Error", () => {
    const ele = test.findByAttr(wrapper, "error-component");
    expect(ele.length).toBe(1);
  });

  it("Should Render Error with Class Name", () => {
    const ele = test.findByVariableAttr(wrapper, "className", "error");
    expect(ele.length).toBe(1);
  });

});
