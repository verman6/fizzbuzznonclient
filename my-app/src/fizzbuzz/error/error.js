import React from 'react';
import PropTypes from 'prop-types';

const Error = (props) => {	
  return (
      <>
	      <div className={props.className} data-test="error-component">
	        <h4>{props.errorMessage}</h4>
	      </div>
      </>
    );
};

Error.defaultProps = {
  className: "",
  errorMessage: ""
};

Error.propTypes = {
  className: PropTypes.string.isRequired,
  errorMessage: PropTypes.string.isRequired
};

export default Error;