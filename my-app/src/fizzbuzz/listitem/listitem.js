import React from 'react';
import PropTypes from 'prop-types';

const ListItem = (data) => {
  const { i, fizz, buzz } = data;
  let li = (i % 3 === 0 && i % 5 === 0) ?
    <li><span className={fizz.toLowerCase()}>{fizz}</span> <span className={buzz.toLowerCase()}>{buzz}</span></li> :
    (i % 3 === 0) ? <li className={fizz.toLowerCase()}>{fizz}</li> :
      (i % 5 === 0) ? <li className={buzz.toLowerCase()}>{buzz}</li> :
        <li>{i}</li>;
  return li;
};

ListItem.defaultProps = {
  i: 0,
  fizz: "",
  buzz: ""
};

ListItem.propTypes = {
  i: PropTypes.number.isRequired,
  fizz: PropTypes.string.isRequired,
  buzz: PropTypes.string.isRequired
};

export default ListItem;