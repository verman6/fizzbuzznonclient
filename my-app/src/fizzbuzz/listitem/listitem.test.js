import React from 'react';
import List from './listitem';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';
import { fetchWord } from '../constants/fizzbuzzcontroller';

describe('List Component', () => {
  let wrapper,wrapper_buzz;
  let data = {
    i: 3,
    fizz: "FIZZ",
    buzz: "BUZZ"
  };
  let data_buzz = {
    i: 5,
    fizz: "FIZZ",
    buzz: "BUZZ"
  };
  beforeEach(() => {
    wrapper = shallow(<List {...data} />);
    wrapper_buzz = shallow(<List {...data_buzz} />);
  });

  it("Should Render One List", () => {
    const el = wrapper.find("li");
    expect(el.length).toBe(1);
  });

  it("Should Render String Fizz", () => {
    const el = wrapper.find("li");
    expect(el.text()).toBe(fetchWord("fizz"));
  });

  it("Should Render String Buzz", () => {
    const el = wrapper_buzz.find("li");
    expect(el.text()).toBe(fetchWord("buzz"));
  });

});
