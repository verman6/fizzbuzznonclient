import React from 'react';
import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';

const Pagination = (data) => {
  const { rows, rowPerPage, handleClick, current } = data;
  const totalPage = Math.ceil(rows.length / rowPerPage);
  const [count, setCount] = useState({
    start: 1,
    end: totalPage > 10 ? 10 : totalPage
  });
  
  useEffect(() => {
    if (totalPage > 10 && current >= 7 && current + 4 <= totalPage) {
      let _start = current - 5;
      let _end = current + 4;
      (_end - _start < 9 && totalPage > 10) ?
        setCount({ start: totalPage - 9, end: totalPage })
        :
        setCount({ start: current - 5, end: current + 4 });
    } else if (totalPage > 10 && current >= 7 && current + 4 > totalPage) {
      let _start = current - 5;
      let _end = totalPage;
      (_end - _start < 9 && totalPage > 10) ?
        setCount({ start: totalPage - 9, end: totalPage })
        :
        setCount({ start: current - 5, end: totalPage });
    } else {
      setCount({ start: 1,
        end: totalPage > 10 ? 10 : totalPage });
    }
  }, [current, rows, totalPage]);

  const pageNumbers = [];
  for (let i = count.start; i <= count.end; i++) {
    pageNumbers.push(
      <li
        key={i}
        id={i}
        onClick={handleClick}
        className = {i === current ? "active" : ""}
      >
        {i}
      </li>
    );
  }

  return (
    <div className="pagination-wrapper" data-test="Pagination-component">
      <h4>Page {current} of {totalPage} </h4>
      <ul className="page-numbers">
        <li id={1} data-test="pagination-li" className={current === 1 ? "disabled First" : "First"} onClick={handleClick}>First</li>
        <li id={current - 1} className={current === 1 ? "disabled Previous" : "Previous"} onClick={handleClick}>Previous</li>
        {pageNumbers}
        <li id={current + 1} className={current === (totalPage) ? "disabled Next" : "Next"} onClick={handleClick}>Next</li>
        <li id={totalPage} className={current === (totalPage) ? "disabled Last" : "Last"} onClick={handleClick}>Last</li>
      </ul>
    </div>
  );

};

Pagination.defaultProps = {
  rows:[],
  rowPerPage: 0,
  current: 0
};

Pagination.propTypes = {
  rows: PropTypes.array.isRequired,
  rowPerPage: PropTypes.number.isRequired,
  current: PropTypes.number.isRequired
};


export default Pagination;