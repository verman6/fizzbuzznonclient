import React from 'react';
import Pagination from './pagination';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Pagination Component', () => {
  let wrapper;
  const onClickMock = jest.fn();
  let rows = [];
  for(let i = 0; i <= 19; i++) {
    rows.push(i);
  }
  let props = {
    rows: rows,
    rowPerPage: 10,
    handleClick: onClickMock,
    current: 2
  };
  beforeEach(() => {
    wrapper = shallow(<Pagination {...props} />);
  });

  it("Should Render Pagination", () => {
    const ele = test.findByAttr(wrapper, "Pagination-component");
    expect(ele.length).toBe(1);
  });

  it("Should Check Rows Length", () => {
    const ele = test.findByAttr(wrapper, "Pagination-component");
    expect(ele.find("li").length).toBe(Math.ceil(props.rows.length / props.rowPerPage) + 4);
  });

  it('Should Trigger OnClick', () => {    
    const ele = test.findByAttr(wrapper, "pagination-li");
    ele.simulate('click');
    expect(onClickMock).toBeCalled();
  });

});