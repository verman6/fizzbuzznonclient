import React, { useState } from 'react';
import { fetchWord } from '../constants/fizzbuzzcontroller';
import ListItem from '../listitem/listitem';
import Pagination from '../pagination/pagination';
import * as Constants from '../constants/constants';
import PropTypes from 'prop-types';

function FizzbuzzResults(props) {
  
  let {value} = props;
  let rowPerPage = Constants.ROWPERPAGE;
  let fizz = fetchWord(Constants.mfizz);
  let buzz = fetchWord(Constants.mbuzz)

  let [myState, setState] = useState({      
      current: Constants.currentPage // Current page for pagination
  });

  const setValue = (ncurrent) => {
    setState({ current: ncurrent }); // current page state update
  }

  // meethod to trigger current page state update
  const handleClick = (e) =>  {
    setValue(Number(e.target.id));
  }

  // method to fetch fizz buzz list for user value returned in List
  let getRows = (value, fizz, buzz) => {
      var List = [];
      for (var i = 1; i <= value; i++) {
        List.push(<ListItem key={i} {...{ i, fizz, buzz }} />);
      }
      return List;
    }

  // row break up based on rowPerPage and current-page pagination value. 
  let breakRows = (current, rowPerPage, ListItem) => {
    let LastRow = current * rowPerPage;
    let FirstRow = LastRow - rowPerPage;
    return ListItem.slice(FirstRow, LastRow);
  }

    if (value === 0) return null;

    let rows = getRows(value, fizz, buzz);
    let currentRows = breakRows(myState.current, rowPerPage, rows);

    return (
      <div data-test="List-component">
        <ul className="list-ul">{currentRows}</ul>
        {(value > rowPerPage) &&
          <Pagination rows={rows} rowPerPage={rowPerPage} handleClick={handleClick} current={myState.current} />
        }
      </div>
    );
}

FizzbuzzResults.defaultProps = {
  value: 0
};

FizzbuzzResults.propTypes = {
  value: PropTypes.number.isRequired
};

export default FizzbuzzResults;