import React from 'react';
import FizzbuzzResults from './fizzbuzzresults';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Fizzbuzz Results Component', () => {
  let wrapper;
  let wrapper_nopagination;
  let wrapper_pagination;
  beforeEach(() => {
    wrapper = shallow(<FizzbuzzResults value={30} />);
    wrapper_nopagination = shallow(<FizzbuzzResults value={5} />);
    wrapper_pagination = shallow(<FizzbuzzResults value={600} />);
  });

  it("Should Render Fizzbuzz Results", () => {
    const ele = test.findByAttr(wrapper, "List-component");
    expect(ele.length).toBe(1);
  });

  it("Should Render Fizzbuzz Results without Pagination", () => {
    const ele = test.findChild(wrapper_nopagination, "Pagination");
    expect(ele.length).toBe(0);
  });

  it("Should Render Fizzbuzz Results with Pagination", () => {
    const ele = test.findChild(wrapper_pagination, "Pagination");
    expect(ele.length).toBe(1);
  });

});
