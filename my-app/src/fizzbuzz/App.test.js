import React from 'react';
import App from './App';
import * as test from '../test/testglobal';
import { shallow } from 'enzyme';

describe('App Component', () => {
  let wrapper;
    beforeEach(() => {
      wrapper = shallow(<App />);
    });

    it("Should Render Header", () => {
      const ele = test.findChild(wrapper, "header");
      expect(ele.length).toBe(1);
    });

    it("Should Render fizzbuzz App", () => {
      const ele = test.findByAttr(wrapper, "fizzbuzz-app-component");
      expect(ele.length).toBe(1);
    });

    it("Should Render fizzbuzz Form", () => {
      const ele = test.findChild(wrapper, "FormComponent");
      expect(ele.length).toBe(1);
    });

});