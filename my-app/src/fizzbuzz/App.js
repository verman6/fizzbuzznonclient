import React from 'react';
import './App.css';
import PageHeader from '../ui-component/header/header';
import FizzbuzzForm from './fizzbuzz-form/fizzbuzz-form';
import * as Validation from './constants/constants';

function App() {

  return (
      <div className="app" data-test="fizzbuzz-app-component">
          <PageHeader className={Validation.headerClass} pageLabel={Validation.headerLabel} />
          <section className="container">
            <FizzbuzzForm  />            
          </section>        
      </div>
    );
}

export default App;
