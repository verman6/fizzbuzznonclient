import React, { useState } from 'react';
import Input from '../../ui-component/input/input';
import Button from '../../ui-component/button/button';
import * as Constants from '../constants/constants';
import FizzbuzzResults from '../fizzbuzzresults/fizzbuzzresults';
import Error from '../error/error';

function FormComponent() {

	let [myState, setMyState] = useState({
		value: Constants.DEFAULT_VAL, // default value for fizz buzz app
		isError: Constants.defaultErrorFlag, // default error display flag
		errorMessage: Constants.defaultErrorMessage, // default error message
		resultFlag: Constants.resultFlag // flag with error flag used for displaying resultant list 
	});

	//Method to update states
	const setValue = (nvalue, nisError, nerrorMessage, nresultFlag) => {
		setMyState({ ...myState, value: nvalue,isError:nisError, errorMessage: nerrorMessage, resultFlag: nresultFlag });
	}

	// Method triggered on user input
	const onInputChange = (e) => {
		//update status with new value
		setValue(Number(e.target.value), Constants.defaultErrorFlag, Constants.defaultErrorMessage, Constants.resultFlag);
	}

	// Method triggered on button click
	const onButtonClick = (e) => {
		const val = myState.value;
		let renderUpdate = {};
		//error handling
		(!Number.isInteger(val))?
			renderUpdate = { isError: true, errorMessage: Constants.INTEGER_NUMBER, resultFlag: Constants.resultFlag }:
				(val < Constants.DEFAULT_VAL)?
					renderUpdate = { isError: true, errorMessage: Constants.POSITIVE_INTEGER, resultFlag: Constants.resultFlag }:
						(val < Constants.MIN_VAL || val > Constants.MAX_VAL)?
							renderUpdate = { isError: true, errorMessage: Constants.INPUTRANGE(Constants.MIN_VAL,Constants.MAX_VAL), resultFlag: Constants.resultFlag }:
							renderUpdate = { isError: Constants.defaultErrorFlag, errorMessage: "", resultFlag: true };
		//states update post user interaction
		setValue(val, renderUpdate.isError, renderUpdate.errorMessage, renderUpdate.resultFlag);
	}

	return(
			<div className="form-wrapper" data-test="form-component">            
	             <Input name="app-input" type="number" className="input" labetText="Enter a number:" onChange={onInputChange} />
	             <Button name="app-button" type="button" className="button" text="Submit" onClick={onButtonClick} />            
	             {myState.isError
		           &&
		           <Error className={ Constants.errorClass } errorMessage={myState.errorMessage} />
		         }
		         {!myState.isError && myState.resultFlag && 
	               <FizzbuzzResults
	                value={myState.value}
	               />
	             }
	         </div>
			);
}

export default FormComponent;