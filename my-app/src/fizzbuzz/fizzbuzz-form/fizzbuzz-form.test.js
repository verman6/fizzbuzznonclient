import React from 'react';
import FizzBuzzForm from './fizzbuzz-form';
import * as test from '../../test/testglobal';
import { shallow } from 'enzyme';

describe('Form Component', () => {
  let wrapper;
  const setState = jest.fn();
  const useStateSpy = jest.spyOn(React, 'useState')
  useStateSpy.mockImplementation((init) => [init, setState]);

  beforeEach(() => {
    wrapper = shallow(<FizzBuzzForm />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Should Render Form Component', () => {
    const ele = test.findByAttr(wrapper, "form-component");
    expect(ele.length).toBe(1);
  });

  it('should have an text field', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-input");
    expect(ele.length).toBe(1);
  });

  it('should have an button field', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-button");
    expect(ele.length).toBe(1);
  });

  it('Input value 0 displays Error for Range Integer with lower limit', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-input");
    ele.props().onChange({target: {value: 0}});
    wrapper.childAt(1).dive().simulate('click');
    wrapper.update();
    const ele1 = test.findChild(wrapper, "Error");
    expect(ele1.length).toBe(1);
  });

  it('Input Value -1 displays Error for Range Integer with higher limit', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-input");
    ele.props().onChange({target: {value: 1001}});
    wrapper.childAt(1).dive().simulate('click');
    wrapper.update();
    const ele1 = test.findChild(wrapper, "Error");
    expect(ele1.length).toBe(1);
  });

  it('Input Value -1 displays Error for Negative Integer', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-input");
    ele.props().onChange({target: {value: -1}});
    wrapper.childAt(1).dive().simulate('click');
    wrapper.update();
    const ele1 = test.findChild(wrapper, "Error");
    expect(ele1.length).toBe(1);
  });

  it('Valid Input Value displays FizzBuzz Results', () => {
    const ele = test.findByVariableAttr(wrapper, "name", "app-input");
    ele.props().onChange({target: {value: 999}});
    wrapper.childAt(1).dive().simulate('click');
    wrapper.update();
    const ele1 = test.findChild(wrapper, "FizzbuzzResults");
    expect(ele1.length).toBe(1);
  });

});