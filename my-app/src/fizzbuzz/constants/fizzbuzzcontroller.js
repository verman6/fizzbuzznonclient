import * as Constants from '../constants/constants';

export const fetchWord = (value) => {
  let val = "";
  let today = new Date().getDay();

  val = (today !== Constants.DAYCONTROL)?value:
            (value === Constants.mfizz)?Constants.mwizz:
            (value === Constants.mbuzz)?Constants.mwuzz:value;
  
  return val.toUpperCase();
};